# Pokemon Android Engine

## Introduction
It is inspired by the second generation of Pokemon games. This Engine is built on top of the e3roid OpenGL Engine for Android allowing for hardware acceleration and easier graphics handling in the code. As far as current completion, it is close to how far my Java Engine was in terms of functionality but many improvements have been made such as authentic damage calculation, proper Pokemon stat generation.

## Completion
Below is what have started working on:

- App Persistence - Done
- Title Screen - Done
- On-Screen Controller - Done
- Hardware Keyboard (If Applicable) Controls - WIP (Overworld only)
- Tile Loading - Done
- Map Loading - Done
- Scrollable Map - Done
- Collision Detection - Done
- Tile based animated movement - Done
- Battle System - WIP (Nearly Finished)
- NPC Interactions - WIP (All you can do is talk to them)
- Day/Night System - WIP (Adding Time based events/encounters later)
- In Game Menu - WIP (Semi-Functional)
- Save/Load - WIP (Only certain things are saved/loaded for now)
- Other misc. systems in place

## Map Editing
This is using the very popular Tiled Map Editor for this engine as it is natively supported by the underlying e3roid OpenGL Engine. This map editor allow for multiple layers (for both tiles, objects, and collisions) and multiple tilesets for each map and is overall just an excellent map editor. You can find the Tiled Map Editor and more information about it [here](http://www.mapeditor.org/).

## To Do list:
- Finish the In Game Menu (Pokedex, Pokemon Party, Bag)
- Finish the Battle System (Items and Party Switching)
- Implement Signs
- Implement Ledges
- Trainer Battles
- Create a short demo up to Violet City and the first Gym
- Clean up the code

![alt tag](https://i.ytimg.com/vi/SAusHvxVpGE/maxresdefault.jpg)

## License
The MIT License (MIT)
Copyright (c) 2015 Flameguru & Aniavasq

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
